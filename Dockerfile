FROM python:3.8-slim-buster

# Create app directory
WORKDIR /python-docker

# Install app dependencies
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

# Bundle app source
COPY . .

EXPOSE 5000

CMD ["python", "-m", "flask","run" , "--host=0.0.0.0"]